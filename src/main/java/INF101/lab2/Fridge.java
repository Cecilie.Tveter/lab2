package INF101.lab2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    ArrayList <FridgeItem> itemsInFridge = new ArrayList<>();
    int fridgeItems = 20;
    @Override
    public int nItemsInFridge() {
        return itemsInFridge.size();
    }

    @Override
    public int totalSize() {
        return fridgeItems;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (itemsInFridge.size() < fridgeItems) {
            itemsInFridge.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        for (int i = 0; i < itemsInFridge.size(); i++) {

            if (itemsInFridge.get(i).equals(item)) {
                itemsInFridge.remove(i);
                return;
            }
        }
        throw new NoSuchElementException();

    }

    @Override
    public void emptyFridge() {
        itemsInFridge.clear();

    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired = new ArrayList<>();
        for (FridgeItem fooditem: itemsInFridge) {
            if (fooditem.hasExpired()) {
                expired.add(fooditem);
            }

        }
        for (FridgeItem badfood:expired ){
            itemsInFridge.remove(badfood);
        }
        return expired;
    }
}
